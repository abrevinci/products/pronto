﻿using Pronto.Core;

namespace Pronto;

public class CommandHandler : IDisposable
{
    private readonly IDisposable _subscription;

    public CommandHandler(IObservable<Command> commands, Action<Message> dispatch)
    {
        _subscription = commands.Subscribe(async command => await ExecuteAsync(command, dispatch));
    }

    private Task ExecuteAsync(Command command, Action<Message> dispatch) =>
        command switch
        {
            _ => throw new NotFiniteNumberException()
        };

    public void Dispose()
    {
        _subscription.Dispose();
    }
}
