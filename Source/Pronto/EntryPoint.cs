﻿using AbreVinci.Embryo;
using AbreVinci.Embryo.WPF;
using Pronto.Core;
using Pronto.ViewModels;

namespace Pronto;

public static class EntryPoint
{
    [STAThread]
    public static void Main()
    {
        try
        {
            var application = new Application();

            var program = Program.Create<State, Message, Command>(Reducer.Init, Reducer.Update, DispatcherScheduler.Current);

            using var commandHandler = CreateCommandHandler(program.Commands, program.DispatchMessage);

            application.MainWindow = CreateMainWindow(CreateMainWindowViewModel(program.States, program.DispatchMessage));
            application.Run();
        }
        catch (Exception exception)
        {
            // put any crash handling code here
            Debug.WriteLine(exception.Message);
        }
    }

    private static CommandHandler CreateCommandHandler(IObservable<Command> commands, Action<Message> dispatch)
    {
        return new CommandHandler(commands, dispatch);
    }

    private static MainWindowViewModel CreateMainWindowViewModel(IObservable<State> state, Action<Message> dispatch)
    {
        return new MainWindowViewModel();
    }

    private static Window CreateMainWindow(MainWindowViewModel mainWindowViewModel)
    {
        var viewResources = new ViewResourceDictionary();

        var window = new Window
        {
            Resources = { MergedDictionaries = { viewResources } },
            ContentTemplate = viewResources.ResolveDataTemplate("MainWindowView"),
            Title = "Pronto",
            WindowState = WindowState.Normal,
            WindowStartupLocation = WindowStartupLocation.CenterScreen,
            Width = 1200,
            Height = 800,
            Content = mainWindowViewModel,
            Visibility = Visibility.Visible
        };

        window.Closed += (_, _) => mainWindowViewModel.Dispose();

        return window;
    }
}
