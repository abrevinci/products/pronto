﻿namespace Pronto.Core;

public static class CommandUtilityExtensions
{
	public static (TState, Command?) WithOptionalCommand<TState>(this TState state, Command? command) => (state, command);
	public static (TState, Command) WithCommand<TState>(this TState state, Command command) => (state, command);
	public static (TState, IImmutableList<Command>) WithCommands<TState>(this TState state, params Command[] commands) => (state, ImmutableArray.Create(commands));
	public static (TState, IImmutableList<Command>) WithNoCommands<TState>(this TState state) => (state, ImmutableArray<Command>.Empty);
	public static (TState, IImmutableList<Command>) QueueCommand<TState>(this (TState, Command?) pair) => (pair.Item1, pair.Item2 != null ? ImmutableArray.Create(pair.Item2) : ImmutableArray<Command>.Empty);
	public static (TState, IImmutableList<Command>) QueueCommand<TState>(this TState state, Command? command) => (state, command != null ? ImmutableArray.Create(command) : ImmutableArray<Command>.Empty);
	public static (TState, IImmutableList<Command>) QueueCommands<TState>(this (TState, IImmutableList<Command>) pair) => pair;
}
