﻿namespace Pronto.Core;

public abstract record Command
{
    private Command () { }
}
