﻿namespace Pronto.Core;

public static class Reducer
{
    public static (State, IImmutableList<Command>) Init() => new State().WithNoCommands();

	public static (State, IImmutableList<Command>) Update(State state, Message message) =>
		message switch
		{
			_ => state.WithNoCommands()
		};
}
