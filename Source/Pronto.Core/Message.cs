﻿namespace Pronto.Core;

public abstract record Message
{
    private Message () { }
}
